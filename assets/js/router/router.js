/*global Backbone */
var app = app || {};

(function () {
	'use strict';

	// Todo Router
	// ----------
	var OauthRouter = Backbone.Router.extend({
    initialize: function() {
			this.loginView = new app.LoginView();
			this.timeLine = new app.TimeLineView();
    },
    
		routes: {
      "": "displayTimeline"
		},

    displayTimeline: function () {
			this.loginView.render();
			var self = this;
      this.timeLine.render();
			if (window.location.search.substring(1) !== "") {
				window.setInterval(function() {
					console.log("In intervall");
		      $.ajax({
		        type: "GET",
		        url: "update",
		        success: function(data) {
		          console.log(data);
		        },
		        error: function(xhr) {
		        }
		      });
				}, 3000);
				window.setInterval(function() {
					self.timeLine.render();
				});
			}
    }
	});
	
	app.router = new OauthRouter();
	Backbone.history.start();
})();
