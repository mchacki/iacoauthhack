var app = app || {};

$(function () {
	'use strict';
  
  app.Users = Backbone.Collection.extend({
    model: app.User,
    url: "listUsers",
    
    initialize: function() {
      this.columns = [
        {name: "screen_name", type: "String"},
        {name: "followers", type: "number"},
        {name: "lastCheck", type: "String"}
      ];
      this.typeOfColumn = {
        screen_name: "String",
        followers: "number",
        lastCheck: "String"
      };
    },
    
    getTypeOfColumn: function(name) {
      return this.typeOfColumn[name];
    },
    
    getColumns: function() {
      return this.columns;
    }
    
  });
  
}());


