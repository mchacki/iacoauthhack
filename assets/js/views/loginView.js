var app = app || {};

$(function () {
	'use strict';

	app.LoginView = Backbone.View.extend({

    template: new EJS({url: 'templates/loginView.ejs'}),
    
    el: "#signIn",
    
		events: {
			"click #signIn": "signIn",
			"click #update": "update",
		},
    
    signIn: function() {
      $.ajax({
        type: "GET",
        url: "oauth",
        dataType: "json",
        processData: false,
        success: function(data) {
          if (!!data.location) {
            window.location.replace(data.location);
          }
        },
        error: function(xhr) {
        }
      });
    },
		
		update: function() {
      $.ajax({
        type: "GET",
        url: "update",
        success: function(data) {
        },
        error: function(xhr) {
        }
      });
		},
		
		render: function () {
			$(this.el).html(this.template.render({}));
			return this;
		}
	});
}());
