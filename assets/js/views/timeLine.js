var app = app || {};

$(function () {
	'use strict';

	app.TimeLineView = Backbone.View.extend({

    template: new EJS({url: 'templates/timeLine.ejs'}),
    
    el: "#content",

		events: {
			"mouseover a": "showDetails",
			"mouseout a": "hideDetails"
		},

		initialize: function() {
			this.blocked = false;
		},

    showDetails: function(event) {
			$(event.currentTarget).popover("show");
			this.blocked = true;
		},
		
		hideDetails: function(event) {
			$(event.currentTarget).popover("hide");
			this.blocked = false;
		},
		
		// Re-render the titles of the todo item.
		render: function () {
			if (!this.blocked) {
				var self = this;
	      $.ajax({
	        type: "GET",
	        url: "timeline",
	        success: function(data) {
						data = _.sortBy(data, "time").reverse();
						this.data = data;
			      $(self.el).html(self.template.render({data: data}));
			      var canvas = document.getElementById('myCanvas');
			      var context = canvas.getContext('2d');
			      context.beginPath();
			      context.moveTo(50, 130);
			      context.lineTo(750, 130);
			      context.lineTo(740, 125);
			      context.moveTo(750, 130);
			      context.lineTo(740, 135);
					  var index = 0;
					  var c;
					  for (c = 674; c > 60 && index < data.length; c -= 64) {
							context.moveTo(c, 110);
							context.lineTo(c + 16, 130);
							context.lineTo(c + 32, 110);
							index++;
						}
						context.stroke();
	        },
	        error: function(xhr) {
	        }
	      });
			}
			return this;
		}
	});
}());
