var app = app || {};

$(function () {
	'use strict';

	app.CrawlView = Backbone.View.extend({

    template: new EJS({url: 'templates/crawlView.ejs'}),
    
    el: "#content",
    
		events: {
			"click #startCrawl": "crawl"
		},

    crawl: function() {
      var tag = $("#crawlTag").val();
      if (tag !== "") {
        $.ajax({
          type: "GET",
          url: "crawl/" + tag,
          processData: false,
          success: function() {},
          error: function() {}
        });
      }
    },
    
		render: function () {
      $(this.el).html(this.template.text);
      this.delegateEvents();
			return this;
		}
	});
}());
