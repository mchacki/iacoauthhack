var app = app || {};

$(function () {
	'use strict';

	app.NaviView = Backbone.View.extend({

    template: new EJS({url: 'templates/naviView.ejs'}),
    
    el: "#navi",

		events: {
			"click li": "navigate",
      "click #signIn": "signIn"
		},

    navigate: function(event) {
      app.router.navigate($(event.currentTarget).attr("id"), {trigger: true});
    },
    
    signIn: function() {
      $.ajax({
        type: "GET",
        url: "oauth",
        dataType: "json",
        processData: false,
        success: function(data) {
          if (!!data.location) {
            window.location.replace(data.location);
          }
        },
        error: function(xhr) {
        }
      });
    },
    
		// Re-render the titles of the todo item.
		render: function (selection) {
      $(this.el).html(this.template.render({
        selected: selection
      }));
			return this;
		}
	});
}());
