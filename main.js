(function() {
  "use strict";

  // Initialise a new FoxxApplication called app under the urlPrefix: "ayeaye".
  var FoxxApplication = require("org/arangodb/foxx").Application,
    app = new FoxxApplication();

  // Register a repository
  app.registerRepository(
    "tokens", { repository: "repositories/tokens"}
  );
  
  app.registerRepository(
    "tweets", { repository: "repositories/tweets"}
  );
	
  app.registerRepository(
    "writes", { repository: "repositories/writes"}
  );
	
  app.registerRepository(
    "users", { repository: "repositories/users"}
  );
	
  // Define a GET event for the URL: prefix + /todos
  // This is used to retrieve the complete list of Todos.
  app.get('/login', function (req, res) {
    // Return the complete content of the Todos-Collection
    //res.json();
    var internal = require('internal');
    var headers = require('lib/oauth').create_header();
    //res.json(headers);
    var options = {
      method:'POST',
      headers:headers
    };
    res.json(internal.download('https://api.twitter.com/oauth/request_token', '', options));
  });
  
  
  app.get('/oauth', function(req, res) {
    var Api = require("lib/twitterApi").TwitterAPI;
    var api = new Api(repositories.tokens.retrieveToken());
    res.json({
      location: "https://api.twitter.com/oauth/authenticate?oauth_token=" + api.requestToken().oauth_token
    })
  });
  
  app.get('/oauth/oauthCallback', function (req, res) {
    var token = req.params("oauth_token");
    var verifier = req.params("oauth_verifier");
    var Api = require("lib/twitterApi").TwitterAPI;
    var api = new Api(repositories.tokens.retrieveToken());
    var verifiedtoken = api.getAccessToken(token, verifier);
    repositories.tokens.insertToken(verifiedtoken);
    res.status(303);
    res.set("Location", "/oauth/index.html?loggedIn=" + verifier);
  });

	app.get("/update", function (req, res) {
    var Api = require("lib/twitterApi").TwitterAPI;
    var api = new Api(repositories.tokens.retrieveToken());
		api.getTweetsForIACologne(function(tweets) {
      var _ = require("underscore");
      _.each(tweets, function(t) {
        var user = t.user;
        var uid = user.id;
        delete t.user;
				repositories.tweets.insertTweet(t);
				repositories.users.insertUser(user);
				repositories.writes.insertWrite(uid, t.id);
			});
		});
	});
	
	app.get("/timeline", function (req, res) {
		var users = repositories.users.collection.name();
		var writes = repositories.writes.collection.name();
		var query = "for u in ";
		query += users;
		query += " return TRAVERSAL(";
		query += users;
		query += ", ";
		query += writes;
		query += ", u._id , ";
		query += "\"outbound\"";
		query += ", { strategy: \"depthfirst\"})";
		//query += "trackPaths: true })";
		var db = require("internal").db;
		var result = db._query(query)._documents;
		var _ = require("underscore");
		var response = [];
		_.each(result, function(r) {
			var uBase = r[0].vertex;
			var u = {};
			var tBase = r[1].vertex;
			var t = {};
			u.name = uBase.name;
			u.image_url = uBase.image_url;
			t.text = tBase.text;
			var time = tBase.date;
			response.push({
				user: u,
				tweet: t,
				time: time
			});
		}); 
		res.json(response);
	});

  /*// Define a POST event for the URL: prefix + /todos
  // This is used to create a new Todo.
  app.post('/todos', function (req, res) {
    var content = JSON.parse(req.requestBody),
      todo = new repositories.todos.modelPrototype(content);
    // Trigger the save event of the model with
    // the given Request Body and return the result.
    res.json(repositories.todos.save(todo));
  }).nickname("todos")
  .summary("Create a new Todo")
  .notes("Creates a new Todo-Item. The information has to be in the requestBody."); 

  // Define a PUT event for the URL: prefix + /todos/:todoid
  // This is used to update an existing Todo.
  app.put("/todos/:id", function (req, res) {
    var id = req.params("id"),
      content = JSON.parse(req.requestBody),
      todo = new repositories.todos.modelPrototype(content);
    // Trigger the update event of the model with
    // the given Request Body and id.
    // Then return the result.
    res.json(repositories.todos.update(id, todo));
  }).nickname("todos")
  .summary("Update a Todo")
  .notes("Changes a Todo-Item. The information has to be in the requestBody."); 
  

  // Define a DELETE event for the URL: prefix + /todos/:todoid
  // This is used to remove an existing Todo.
  app['delete']("/todos/:id", function (req, res) {
    var id = req.params("id");
    // Trigger the remove event in the collection with
    // the given id and return the result.
    res.json(repositories.todos.destroy(id));
  }).nickname("todos")
  .pathParam("id", {
    description: "The id of the Todo-Item",
    dataType: "string",
    required: true,
    multiple: false
  })
  .summary("Removes a Todo")
  .notes("Removes a Todo-Item."); 
  */

  // Start the todo-list application.
  // Remember to give the applicationContext.
  app.start(applicationContext);
}());