/*jslint indent: 2, nomen: true, maxlen: 100, white: true, plusplus: true, unparam: true */
/*global require, exports*/

////////////////////////////////////////////////////////////////////////////////
/// @brief A Twitter crawler
///
/// @file This Document represents the repository communicating with ArangoDB
///
/// DISCLAIMER
///
/// Copyright 2010-2012 triagens GmbH, Cologne, Germany
///
/// Licensed under the Apache License, Version 2.0 (the "License");
/// you may not use this file except in compliance with the License.
/// You may obtain a copy of the License at
///
///     http://www.apache.org/licenses/LICENSE-2.0
///
/// Unless required by applicable law or agreed to in writing, software
/// distributed under the License is distributed on an "AS IS" BASIS,
/// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
/// See the License for the specific language governing permissions and
/// limitations under the License.
///
/// Copyright holder is triAGENS GmbH, Cologne, Germany
///
/// @author Michael Hackstein
/// @author Copyright 2011-2013, triAGENS GmbH, Cologne, Germany
////////////////////////////////////////////////////////////////////////////////



(function () {
  "use strict";
  var _ = require("underscore"),
    db = require("internal").db,
    Foxx = require("org/arangodb/foxx"),
    Writes_Repository = Foxx.Repository.extend({
      insertWrite: function(userId, tweetId) {
        var col = this.collection,
		    prefix = this.prefix,
		    createKey = function(s, t) {
		      return "writes_" + s + "_" + t;
		    },
		    createUserId = function(key) {
		      return prefix + "_users/user_" + key;
		    },
		    createTweetId = function(key) {
		      return prefix + "_tweets/tweet_" + key;
		    },
				key = createKey(userId, tweetId),
        action = function() {
          try {
            col.document(key);
            return;
          } catch(e) {
            var toAdd = {
              _key: key
            };
            col.save(createUserId(userId), createTweetId(tweetId), toAdd);
          }
        };
        db._executeTransaction({
          collections: {
            write: col.name()
          },
          action: action
        });
      }
    });
  exports.Repository = Writes_Repository;
}());
