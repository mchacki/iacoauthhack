/*jslint indent: 2, nomen: true, maxlen: 100, white: true, plusplus: true, unparam: true */
/*global require, exports*/

////////////////////////////////////////////////////////////////////////////////
/// @brief A Twitter crawler
///
/// @file This Document represents the repository communicating with ArangoDB
///
/// DISCLAIMER
///
/// Copyright 2010-2013 triagens GmbH, Cologne, Germany
///
/// Licensed under the Apache License, Version 2.0 (the "License");
/// you may not use this file except in compliance with the License.
/// You may obtain a copy of the License at
///
///     http://www.apache.org/licenses/LICENSE-2.0
///
/// Unless required by applicable law or agreed to in writing, software
/// distributed under the License is distributed on an "AS IS" BASIS,
/// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
/// See the License for the specific language governing permissions and
/// limitations under the License.
///
/// Copyright holder is triAGENS GmbH, Cologne, Germany
///
/// @author Michael Hackstein
/// @author Copyright 2011-2013, triAGENS GmbH, Cologne, Germany
////////////////////////////////////////////////////////////////////////////////

var internal = require("internal");
var OAuth = require("lib/foxx_oauth").OAuth;
var _ = require("underscore");

exports.TwitterAPI = function(token) {
  "use strict";

  this._oa = new OAuth(
  	"https://api.twitter.com/oauth/request_token",
  	"https://api.twitter.com/oauth/access_token",
  	"qeBHCVtHeWfgxecjjnvoA",
  	"0HPOkamv0agfLXfqYkRMcPzO1soxzgLf8laq5tJfz8",
  	"1.0",
  	"http://localhost:8529/oauth/oauth/oauthCallback",
  	"HMAC-SHA1"
  );
  if (!!token) {
    this._oauth_token = token.oauth_token;
    this._oauth_token_secret = token.oauth_token_secret;
  }
  
  var self = this;
  this._urls = {
    tweets: "https://api.twitter.com/1.1/search/tweets.json?count=100&q=#",
		iacologne: "https://api.twitter.com/1.1/search/tweets.json?count=10&q=#iacologne",
    followers: "https://api.twitter.com/1.1/followers/ids.json?user_id=",
    following: "https://api.twitter.com/1.1/friends/ids.json?user_id=",
    userInfo: "https://api.twitter.com/1.1/users/lookup.json?user_id="
  };
  
  this._shortenUser = function(user) {
    return {
      id: user.id,
      name: user.name,
      screen_name: user.screen_name,
      image_url: user.profile_image_url
    };
  };
  
  this._sendGetRequest = function(url, callback) {
    var method = "GET";
    var header = this._oa.authHeader(url, this._oauth_token, this._oauth_token_secret, method)
    var options = {
      method: method,
      headers: {"Authorization": header}
    };
    var reqRes = internal.download(url, "", options);
    if (reqRes.code === 200) {
      callback(JSON.parse(reqRes.body))
    } else {
      callback({
        code: reqRes.code,
        message: reqRes.message
      });
    }
  };
};


////////////////////////////////////
// Public Functions               //
////////////////////////////////////

exports.TwitterAPI.prototype.requestToken = function() {
  return this._oa.getOAuthRequestToken(function(){});;
};

exports.TwitterAPI.prototype.getAccessToken = function(token, verifier) {
  return this._oa.getOAuthAccessToken(token, verifier);;
};


exports.TwitterAPI.prototype.getTweets = function(tag, callback) {
  var self = this;
  this._sendGetRequest(this._urls.tweets + tag, function(r) {
    if (r.code !== undefined && r.message !== undefined) {
      require("console").log(r.code);
      require("console").log(r.message);
      //callback(r);
      return;
    }
    var result = r.statuses;
    result = _.map(result, function(res) {
      var user = self._shortenUser(res.user);
      return {
        id: res.id,
        user: user,
        date: res.created_at,
        text: res.text,
        tag: tag
      };  
    });
    callback(result);
  });
};

exports.TwitterAPI.prototype.getFollowers = function(id, callback) {
  this._sendGetRequest(this._urls.followers + id, function(r) {
    var list = r.ids || [];
    callback(list);
  });
};

exports.TwitterAPI.prototype.getFollowing = function(id, callback) {
  this._sendGetRequest(this._urls.following + id, function(r) {
    var list = r.ids || [];
    callback(list);
  });
};

exports.TwitterAPI.prototype.lookupUsersList = function(list, callback) {
  var self = this;
  if (list.length === 0) {
    return;
  }
  var arrays = [], length = 100;
  while (list.length > 0) {
    arrays.push(list.splice(0, length));
  }
  _.each(arrays, function(ids) {
    self._sendGetRequest(self._urls.userInfo + ids, function(r) {
      r = _.map(r, function(u) {
        return self._shortenUser(u);
      });
      callback(r);
    });
  });
};

exports.TwitterAPI.prototype.getTweetsForIACologne = function(callback) {
  var self = this;
  this._sendGetRequest(this._urls.iacologne, function(r) {
    if (r.code !== undefined && r.message !== undefined) {
      require("console").log(r.code);
      require("console").log(r.message);
      //callback(r);
      return;
    }
    var result = r.statuses;
    result = _.map(result, function(res) {
      var user = self._shortenUser(res.user);
      return {
        id: res.id,
        user: user,
        date: res.created_at,
        text: res.text,
        tag: "IACologne"
      };  
    });
    callback(result);
  });
	
};
